import numpy as np
from sklearn import preprocessing

def get_input():
    input_data = np.array([[-5.3, -8.9, 3.0], [-2.9, 5.1, -3.3], [3.1, -2.8, -3.2], [2.2, -1.4, 5.1]])
    return input_data

#Бінарізація
def task_2_2_1():
    threshold = 3.0
    input_data = get_input()
    data_binarized = preprocessing.Binarizer(threshold=threshold).transform(input_data)
    return data_binarized

print("\n Бінарізація:\n", task_2_2_1())
#end Бінарізація

#Виключення середнього
def task_2_2_2():
    print("\n\n\n\n\nВиключення середнього: ")

    input_data = get_input()
    print("\nBEFORE: ")
    print("Mean =", input_data.mean(axis=0))
    print("Std deviation =", input_data.std(axis=0))

    data_scaled = preprocessing.scale(input_data)
    print("\nAFTER: ")
    print("Mean =", data_scaled.mean(axis=0))
    print("Std deviation =", data_scaled.std(axis=0))

task_2_2_2()
#end task 2.1.2

#end Виключення середнього

#Масштабування
def task_2_2_3():
    print("\n\n\n\n\nМасштабування: ")
    input_data = get_input()
    data_scaler_minmax = preprocessing.MinMaxScaler(feature_range=(0, 1))
    data_scaled_minmax = data_scaler_minmax.fit_transform(input_data)
    print("\nМin max scaled data:\n", data_scaled_minmax)

task_2_2_3()
#end Масштабування


#Нормалізація
def task_2_2_4():
    print("\n\n\n\n\nНормалізація: ")
    input_data = get_input()
    data_normalized_l1 = preprocessing.normalize(input_data, norm='l1')
    data_normalized_l2 = preprocessing.normalize(input_data, norm='l2')
    print("\nl1 normalized data:\n", data_normalized_l1)
    print("\nl2 normalized data:\n", data_normalized_l2)


task_2_2_4()
#end Нормалізація

